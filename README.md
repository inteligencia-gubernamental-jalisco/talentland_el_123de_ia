# Taller: El 1, 2, 3 de la Inteligencia Artificial

## E. Ulises Moya Sánchez & Abraham Sánchez

En este taller se dará una presentación inicial  a  la Inteligencia Artificial (IA) sus alcances, aplicaciones, actuales. 
Adicionalmente, trabajaremos en prácticas de redes neuronales artificiales.

## Objetivos del taller

- Entender  cuáles son las técnicas más usadas en IA. 
- Explorar algunos ejemplos con redes neuronales artificiales

##  ¿Cómo trabajar con Google Colaboratory?

Para crear un archivo de Google Colaboratory sigue los siguientes pasos:

_Nota: requerido tener una cuenta de Google._

1. Ir al siguiente sitio: https://colab.research.google.com/notebooks/intro.ipynb. Aparece lo siguiente:

![](https://gitlab.com/inteligencia-gubernamental-jalisco/riiaa20/uploads/bd23cca5fbb30165aa1928bad0bae0c1/google_colaboratory_main.PNG)

2. Dar clic en `Archivo` (File) > `Subir notebook` (Upload). Aparece la siguiente ventana.

![](https://gitlab.com/inteligencia-gubernamental-jalisco/talentland_el_123de_ia/uploads/3a18a10b49bc13136cbae43bca0e34c2/upload_notebook.png)

3. Arrastrar o da clic en el botón `buscar` (browse) el archivo notebook (.ipynb) (archivos almacenados en la carpeta `notebooks`).